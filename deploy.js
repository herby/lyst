define([
    'amber/deploy',
    // --- packages to be deployed begin here ---
    "lyst/Lyst"
    // --- packages to be deployed end here ---
], function (amber) {
    return amber;
});
