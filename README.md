Lyst
====
Get / set hierarchical data using array-like indexes.

The Lyst index (aka yndex) an array of elements: either strings, numbers
or a sub-arrays. These are used to denote the (relative) location
of a piece of data in a hierarchical object, and is used to read or write
from / to this position.

Elements of a path are equivalent to elements of paths in classic file systems:
each elements is one step deeper in a tree hierarchy. Thus, to read a data denoted
by a path, Lyst starts from actual position, reads the contents denoted by first element,
use the result to read the contents denoted by second elements etc. until the end.
To write the data, the algorithm is similar to reading one, byt the last element is used
to write the data instead.

 - if _string_ path element is read from _foo_, `foo at: aString` is performed;
 - if _string_ path element is written to  _foo_, `foo at: aString put: value` is performed;
 - if _number_ path element is read from _foo_, `foo at: aNumber` is performed;
 - if _number_ path element is written to _foo_, `foo at: aNumber put: value` is performed;
 - if _subarray_ path element `#(bar)` is read from _foo_, `foo bar` is performed;
 - if _subarray_ path element `#(bar)` is written to _foo_, `foo bar: value` is performed.

API
----

----

```st
Object >> atLyst: aCollection ifAbsent: aBlock
```

For example `container atLyst: #((todos) 1 done) ifAbsent: [...]'` essentially does

	| x |
	x := container todos at: 1.
	^ x at: 'done'

But, whenever:

  - `container` fails to perform `todos`, or
  - `container todos` fails to perform `at:ifAbsent:`, or
  - `container todos` does not contain index 1, or
  - `container todos at: 1` fails to perform `at:ifAbsent:`, or
  - `container todos at: 1` does not contain index 'done',

the `ifAbsent` block value is returned.

----

```st
Object >> atLyst: aCollection ifAbsent: aBlock put: anObject
```

For example `container atLyst: #((todos) 1 done) ifAbsent: [...] put: 'foo'` essentially does

	| x |
	x := container todos at: 1.
	^ x at: 'done' put: 'foo'

But, whenever:

  - `container` fails to perform `todos`, or
  - `container todos` fails to perform `at:ifAbsent:`, or
  - `container todos` does not contain index 1, or
  - `container todos at: 1` fails to do `at:put:`,

the `ifAbsent` block value is returned.

----

```st
Lyst class >> parse: aString
```

Parses a string to get a proper array index to use with `atLyst:` API.

The syntax is resembling Smalltalk literal array syntax very closely.
For example `Lyst parse: '(value)'` and `Lyst parse: '(todos) 1 done'`
produce `#((value))` and `#((todos) 1 done)` as results.

Syntactic sugar: as `(foo)` happens often, to denote unary selector,
it can be written equivalently as `~foo`, to improve readability.
So above Lyst indexes' parseable string representation
would likely be written `'~value'` and `'~todos 1 done'` instead.
